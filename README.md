<div align="center"><img src="https://codeberg.org/img/logo.svg" height="300px" align="center"></div>

## 📓 [Community Issue Tracker](https://codeberg.org/Codeberg/Community/issues)

Please use this issue tracker to discuss Codeberg-related issues. It's usually the best place to report things to, unless you know it's directly related to some topic.

## 📖 [Documentation](https://docs.codeberg.org)

You might also want to have a look at the docs on [docs.codeberg.org](https://docs.codeberg.org).
